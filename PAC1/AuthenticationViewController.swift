//
//  AuthenticationViewController.swift
//  PR1
//
//  Copyright © 2018 UOC. All rights reserved.
//

import UIKit

class AuthenticationViewController: UIViewController, UITextFieldDelegate {
    // BEGIN-UOC-4
    @IBOutlet weak var firstField: UITextField!
    @IBOutlet weak var secondField: UITextField!
    @IBOutlet weak var thirdField: UITextField!
    @IBOutlet weak var fourthField: UITextField!
    
    // Strings for the error message
    let errorAuthenticationMessageTitle: String = "Authentication error"
    let errorAuthenticationMessageContent: String = "Sorry, the entered code is not valid"
    
    // String with allowed characters for validation code (numbers)
    let allowedCharacters: String = "0123456789"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Add this ViewController as a delegate for each UITextFields
        firstField.delegate = self
        secondField.delegate = self
        thirdField.delegate = self
        fourthField.delegate = self
    }
    
    // Check UITextField's content when editing it
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // Check if more than one character was introduced
        let currentText = textField.text ?? ""
        let stringRange = Range(range, in: currentText)
        let updatedText = currentText.replacingCharacters(in: stringRange!, with: string)
        if updatedText.count <= 1 {
            if containsAllowedCharacters(typedString: string) {
                return true
            }
        }
        return false
    }
    
    // Check if only allowed characters are introduced
    func containsAllowedCharacters(typedString: String) -> Bool {
        let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
        let typedCharacterSet = CharacterSet(charactersIn: typedString)
        let allowed = allowedCharacterSet.isSuperset(of: typedCharacterSet)
        return allowed
    }
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        // Unwrap the introduced numbers and concatenate them
        if let firstNumber: String = firstField?.text, let secondNumber: String = secondField?.text, let thirdNumber: String = thirdField?.text, let fourthNumber: String = fourthField?.text {
            let enteredCode: String = firstNumber + secondNumber + thirdNumber + fourthNumber
            // Use utils' function to validate the introduced authentication code
            let authorizationAllowed = Services.validate(code: enteredCode)
            if (authorizationAllowed) {
                print("Authentication screen: authentication succeed")
                performSegue(withIdentifier: "SegueToMainNavigation", sender: self)
            } else {
                print("Authentication screen: authentication error")
                Utils.show(Message: errorAuthenticationMessageContent, WithTitle: errorAuthenticationMessageTitle, InViewController: self)
            }
        }
        
    }
    // END-UOC-4
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    } 
}
